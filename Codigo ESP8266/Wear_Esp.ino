#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
 
const char* ssid = "DaltonReis";
const char* password = "12345678";
 
void setup () {
  pinMode(LED_BUILTIN, OUTPUT); 
  Serial.begin(115200);
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
 
    delay(1000);
    //Serial.print("Connecting..");
 
  }
 
}
 
void loop() {
 
  if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status
 
    HTTPClient http;  //Declare an object of class HTTPClient
 
    http.begin("http://dppwear-io.umbler.net/wear/status");  //Specify request destination
    int httpCode = http.GET();                                                                  //Send the request
 
    if (httpCode > 0) { //Check the returning code
 
      String payload = http.getString();   //Get the request response payload
      if (payload == "Status: Desligado.") {
          digitalWrite(LED_BUILTIN, LOW);
      } else {
          digitalWrite(LED_BUILTIN, HIGH);
      }
      /*Serial.println("\n");
      Serial.println("\n");
      Serial.println(payload);                     //Print the response payload
      Serial.println("\n");
      Serial.println("\n");*/
    }
 
    http.end();   //Close connection
 
  }
 
  delay(1000);    //Send a request every 30 seconds
 
}
