﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Extensions;
using Nancy.Hosting.Aspnet;
using Nancy.Hosting;
using System.Collections;

/*
	- Os módulos definem o comportamento das aplicações Nancy;
	- O Nancy escaneia todos os módulos e cacheia depois, não é custoso e os módulos são globais;
	- Utiliza HTTP, suporta DELETE, GET, HEAD, OPTIONS, POST, PUT e PATCH;
    - O DynamicDictionary permite acessar os valores como index ou propriedade (ver em ProductsModule)
*/

namespace servicos_api.Modules
{
    public class Main : NancyModule
    {
        static bool status = true;

        public Main()
        {
            Get["/status"] = _ =>
            {
                if (status)
                {
                    return "Status: Ligado.";
                }
                else
                {
                    return "Status: Desligado.";
                }
            };

            Get["/{acao}"] = parameters =>
            {
                string acao = parameters.acao;
                if (acao == "1")
                {
                    status = !status;
                    return "Alterado status do led";
                } else { 
                    return "Acao " + parameters.acao;
                }
            };
            
        }
        
    }
}
